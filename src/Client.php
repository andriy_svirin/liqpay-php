<?php

namespace UIS\LIQPAY;

use LiqPay;

class Client
{

    private $version = '3';
    private $liqPay;
    private $endpointUrl;

    public function __construct(LiqPay $liqPay, string $endpointUrl = null)
    {
        $this->liqPay = $liqPay;
        $this->endpointUrl = $endpointUrl;
    }

    /**
     * @param string $phone
     * @param int $amount
     * @param string $orderId
     * @param string|null $description
     * @return Result
     * @throws Exception
     */
    public function payCash(string $phone, int $amount, string $orderId, string $description = null)
    {
        $req = new Request([
            'action' => 'paycash',
            'version' => $this->version,
            'phone' => $phone,
            'amount' => $amount,
            'currency' => LiqPay::CURRENCY_UAH,
            'description' => $description ?? 'paycash from site',
            'order_id' => $orderId,
            'server_url' => $this->endpointUrl,
            'sandbox' => 1,
        ]);
        $res = new Response($this->liqPay->api('request', $req->req));
        return $this->handleResponse($req, $res);
    }

    /**
     * @param Request $req
     * @param Response $res
     * @return Result
     * @throws Exception
     */
    private function handleResponse(Request $req, Response $res): Result
    {
        if ('error' === $res->getResult()) {
            throw new Exception($req, $res);
        }
        return new Result($req, $res);
    }
}