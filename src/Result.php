<?php

namespace UIS\LIQPAY;

class Result
{

    /**
     * @var Request
     */
    public $request;

    /**
     * @var \stdClass
     */
    public $res;

    public function __construct(Request $request, Response $res)
    {
        $this->request = $request;
        $this->res = $res;
    }
}