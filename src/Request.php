<?php

namespace UIS\LIQPAY;

class Request
{

    /**
     * @var \stdClass
     */
    public $req;

    public function __construct(array $req)
    {
        $this->req = $req;
    }

}