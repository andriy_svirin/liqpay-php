<?php

namespace UIS\LIQPAY;

class Response
{

    /**
     * @var \stdClass
     */
    public $res;

    public function __construct(\stdClass $res)
    {
        $this->res = $res;
    }

    public function getResult(): string
    {
        return $this->res->result;
    }

    public function getCode(): string
    {
        return $this->res->code;
    }
}