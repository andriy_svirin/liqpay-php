<?php

namespace UIS\LIQPAY;

class Exception extends \Exception
{

    /**
     * @var Request
     */
    public $request;

    /**
     * @var \stdClass
     */
    public $response;

    public function __construct(Request $request, Response $response)
    {
        parent::__construct($response->getCode(), 0, null);
        $this->request = $request;
        $this->response = $response;
    }
}